const types = {
    TOGGLE_BUY_MODAL: "TOGGLE_BUY_MODAL",
    TOGGLE_WISHLIST_MODAL: "TOGGLE_WISHLIST_MODAL",
    ADD_BASKET: "ADD_BASKET",
    ADD_WISHLIST: "ADD_WISHLIST",
    REMOVE_WISHLIST: "REMOVE_WISHLIST",
    REMOVE_BASKET: "REMOVE_BASKET",
    GET_BOOKS: "GET_BOOKS"
}

export default types