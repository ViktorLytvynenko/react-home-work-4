import style from "../Modal.module.scss";
import {useEffect, useRef} from "react";

const Buttons = (props) => {
    const confirmRef = useRef();
    const cancelRef = useRef();
    useEffect(() => {
       confirmRef.current.addEventListener("click", ()=>{
           props.confirm()
       });
       cancelRef.current.addEventListener("click", props.handleClick);
    })
    return (
        <div className={style.modal_body_container}>
            <button className={`${style.modal_body_container_button} ${props.themeStyle}`} ref={confirmRef}>Підтверджую</button>
            <button className={`${style.modal_body_container_button} ${props.themeStyle}`} ref={cancelRef}>Відмінити</button>
        </div>
        )
}

export default Buttons
