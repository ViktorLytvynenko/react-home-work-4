import {
    toggleBuyModal,
    toggleWishListModalModal,
    addBasket,
    addWishList,
    removeWishList,
    removeBasket,
    getBooks} from "./redux/reducers/productsReducer"
import {connect} from "react-redux";
import App from "./App";

const mapStateToProps = (state) => ({
    state: {
            ...state.productsPage
        }
})

const mapDispatchToProps = {
    toggleBuyModal,
    toggleWishListModalModal,
    addBasket,
    addWishList,
    removeWishList,
    removeBasket,
    getBooks
}

export default connect(mapStateToProps, mapDispatchToProps)(App)